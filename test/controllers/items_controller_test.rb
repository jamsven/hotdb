require 'test_helper'

class ItemsControllerTest < ActionController::TestCase
  setup do
    @item = items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post :create, item: { amazon_link: @item.amazon_link, country: @item.country, creator: @item.creator, date_of_purchase: @item.date_of_purchase, description: @item.description, ean_code: @item.ean_code, features: @item.features, format: @item.format, genres: @item.genres, height: @item.height, length: @item.length, rating: @item.rating, retail_price: @item.retail_price, sold_by: @item.sold_by, subtitle: @item.subtitle, synopsis: @item.synopsis, title: @item.title, warranty: @item.warranty, width: @item.width }
    end

    assert_redirected_to item_path(assigns(:item))
  end

  test "should show item" do
    get :show, id: @item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @item
    assert_response :success
  end

  test "should update item" do
    patch :update, id: @item, item: { amazon_link: @item.amazon_link, country: @item.country, creator: @item.creator, date_of_purchase: @item.date_of_purchase, description: @item.description, ean_code: @item.ean_code, features: @item.features, format: @item.format, genres: @item.genres, height: @item.height, length: @item.length, rating: @item.rating, retail_price: @item.retail_price, sold_by: @item.sold_by, subtitle: @item.subtitle, synopsis: @item.synopsis, title: @item.title, warranty: @item.warranty, width: @item.width }
    assert_redirected_to item_path(assigns(:item))
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete :destroy, id: @item
    end

    assert_redirected_to items_path
  end
end
