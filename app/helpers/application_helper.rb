module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Hot Database"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def foundation_flash(flash_type = 'alert')
    
  end
end
