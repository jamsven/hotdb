class Item < ActiveRecord::Base
    validates :title, presence: true, length: { minimum: 5 }
    has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => ":style/missing.png"
    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
