json.array!(@items) do |item|
  json.extract! item, :id, :title, :subtitle, :ean_code, :description, :synopsis, :creator, :rating, :genres, :sold_by, :features, :format, :retail_price, :warranty, :date_of_purchase, :length, :width, :height, :amazon_link, :country, :image
  json.url item_url(item, format: :json)
end
