class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.string :subtitle
      t.string :ean_code
      t.string :description
      t.string :synopsis
      t.string :creator
      t.integer :rating
      t.string :genres
      t.string :sold_by
      t.string :features
      t.string :format
      t.decimal :retail_price
      t.float :warranty
      t.date :date_of_purchase
      t.float :length
      t.float :width
      t.float :height
      t.string :amazon_link
      t.string :country

      t.timestamps null: false
    end
  end
end
